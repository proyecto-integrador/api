package net.dcerp.dao;


import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.stream.Collectors;
import java.util.stream.Stream;


import java.text.SimpleDateFormat;
import java.text.ParseException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import net.dcerp.model.Sale;
import net.dcerp.repository.SaleRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SaleDaoTest {
    @Autowired
    private SaleDAO service;

    @MockBean
    private SaleRepository repository;

    public Date setDate() {

    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    String dateInString = "2020-04-14";

    Date date = new Date();

    try {
        date = formatter.parse(dateInString);
    } catch (ParseException e) {
       e.printStackTrace();
    }

    return date;

    }


    @Test
    public void findAllTest() {
        Date date = this.setDate();
          when(repository.findAll())
                .thenReturn(Stream.of(new Sale(1, "DistriLacteos","cra 63 b 45-60",4522529, date)).collect(Collectors.toList()));
        assertEquals(1, service.findAll().size());
    }

    @Test
    public void saveBuyTest() {
        Date date = this.setDate();
        Sale sale = new Sale(1, "DistriLacteos","cra 63 b 45-60",4522529, date);
        when(repository.save(sale)).thenReturn(sale);
        assertEquals(sale, service.save(sale));
    }

    @Test
    public void deleteBuyTest(){
        Date date = this.setDate();
        Sale sale = new Sale(1, "DistriLacteos","cra 63 b 45-60",4522529, date);
        service.delete(sale);
        verify(repository, times(1)).delete(sale);
    }
}
