package net.dcerp.dao;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import net.dcerp.model.SaleLine;
import net.dcerp.repository.SaleLineRepository;

@RunWith(SpringRunner.class)
@SpringBootTest

public class SaleLineDaoTest {

    @Autowired
    private SaleLineDAO service;

    @MockBean
    private SaleLineRepository repository;

    @Test
    public void findAllTest() {
        when(repository.findAll())
                .thenReturn(Stream.of(new SaleLine(1, 1030,"Leche Deslactosada", 2500, 100)).collect(Collectors.toList()));
        assertEquals(1, service.findAll().size());
    }

    @Test
    public void saveSaleLineTest() {
        SaleLine saleLine = new SaleLine(1, 1030,"Leche Deslactosada", 2500, 100);
        when(repository.save(saleLine)).thenReturn(saleLine);
        assertEquals(saleLine, service.save(saleLine));
    }

    @Test
    public void deleteSaleLineTest(){
        SaleLine saleLine = new SaleLine(1, 1030,"Leche Deslactosada", 2500, 100);
        service.delete(saleLine);
        verify(repository, times(1)).delete(saleLine);
    }
}