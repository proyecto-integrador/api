package net.dcerp.dao;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import net.dcerp.model.BuyLine;
import net.dcerp.repository.BuyLineRepository;

@RunWith(SpringRunner.class)
@SpringBootTest

public class BuyLineDaoTest {

    @Autowired
    private BuyLineDAO service;

    @MockBean
    private BuyLineRepository repository;

    @Test
    public void findAllTest() {
        when(repository.findAll())
                .thenReturn(Stream.of(new BuyLine(1, 1020,"Yogurt Griego", 3600, 100)).collect(Collectors.toList()));
        assertEquals(1, service.findAll().size());
    }

    @Test
    public void saveBuyLineTest() {
        BuyLine buyLine = new BuyLine(1, 1020,"Yogurt Griego", 3600, 100);
        when(repository.save(buyLine)).thenReturn(buyLine);
        assertEquals(buyLine, service.save(buyLine));
    }

    @Test
    public void deleteBuyLineTest(){
        BuyLine buyLine = new BuyLine(1, 1020,"Yogurt Griego", 3600, 100);
        service.delete(buyLine);
        verify(repository, times(1)).delete(buyLine);
    }
}
