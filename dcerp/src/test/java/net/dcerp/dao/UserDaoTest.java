package net.dcerp.dao;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import net.dcerp.model.User;
import net.dcerp.repository.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserDaoTest {

    @Autowired
    private UserDAO service;

    @MockBean
    private UserRepository repository;

    @Test
    public void findAllTest() {
        when(repository.findAll())
                .thenReturn(Stream.of(new User(1, "Sebastian Munoz", "1234")).collect(Collectors.toList()));
        assertEquals(1, service.findAll().size());
    }

    @Test
    public void saveUserTest() {
        User user = new User(1, "Sebastian Munoz", "1234");
        when(repository.save(user)).thenReturn(user);
        assertEquals(user, service.save(user));
    }

    @Test
    public void deleteUserTest(){
        User user = new User(1, "Sebastian Munoz", "1234");
        service.delete(user);
        verify(repository, times(1)).delete(user);
    }
}
