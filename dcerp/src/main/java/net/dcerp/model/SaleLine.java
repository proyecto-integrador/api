package net.dcerp.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;


/**
 *
 * @author David Caballero <d@dcaballero.net>
 */
@Entity
@Table(name = "sale_line", catalog = "dcerp", schema = "")
@EntityListeners(AuditingEntityListener.class)
@NamedQueries({
    @NamedQuery(name = "SaleLine.findAll", query = "SELECT s FROM SaleLine s")
    , @NamedQuery(name = "SaleLine.findById", query = "SELECT s FROM SaleLine s WHERE s.id = :id")
    , @NamedQuery(name = "SaleLine.findByIdProduct", query = "SELECT s FROM SaleLine s WHERE s.idProduct = :idProduct")
    , @NamedQuery(name = "SaleLine.findByProductName", query = "SELECT s FROM SaleLine s WHERE s.productName = :productName")
    , @NamedQuery(name = "SaleLine.findByProductCost", query = "SELECT s FROM SaleLine s WHERE s.productCost = :productCost")
    , @NamedQuery(name = "SaleLine.findByProductQuantity", query = "SELECT s FROM SaleLine s WHERE s.productQuantity = :productQuantity")})
public class SaleLine implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "id_product")
    private int idProduct;
    @Basic(optional = false)
    @Column(name = "product_name")
    private String productName;
    @Basic(optional = false)
    @Column(name = "product_cost")
    private int productCost;
    @Basic(optional = false)
    @Column(name = "product_quantity")
	private int productQuantity;
	@JsonIgnore
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Sale sale;

    public SaleLine() {
    }

    public SaleLine(Integer id) {
        this.id = id;
    }

    public SaleLine(Integer id, int idProduct, String productName, int productCost, int productQuantity) {
        this.id = id;
        this.idProduct = idProduct;
        this.productName = productName;
        this.productCost = productCost;
        this.productQuantity = productQuantity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductCost() {
        return productCost;
    }

    public void setProductCost(int productCost) {
        this.productCost = productCost;
    }

    public int getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
    }

    public Sale getSale() {
        return sale;
    }

    public void setSale(Sale sale) {
        this.sale = sale;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof SaleLine)) {
            return false;
        }
        SaleLine other = (SaleLine) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication3.SaleLine[ id=" + id + " ]";
    }

}