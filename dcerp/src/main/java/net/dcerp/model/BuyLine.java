package net.dcerp.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author David Caballero <d@dcaballero.net>
 */
@Entity
@Table(name = "buy_line", catalog = "dcerp", schema = "")
@EntityListeners(AuditingEntityListener.class)
@NamedQueries({
    @NamedQuery(name = "BuyLine.findAll", query = "SELECT b FROM BuyLine b")
    , @NamedQuery(name = "BuyLine.findById", query = "SELECT b FROM BuyLine b WHERE b.id = :id")
    , @NamedQuery(name = "BuyLine.findByIdProduct", query = "SELECT b FROM BuyLine b WHERE b.idProduct = :idProduct")
    , @NamedQuery(name = "BuyLine.findByProductName", query = "SELECT b FROM BuyLine b WHERE b.productName = :productName")
    , @NamedQuery(name = "BuyLine.findByProductCost", query = "SELECT b FROM BuyLine b WHERE b.productCost = :productCost")
    , @NamedQuery(name = "BuyLine.findByProductQuantity", query = "SELECT b FROM BuyLine b WHERE b.productQuantity = :productQuantity")})
public class BuyLine implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "id_product")
    private int idProduct;
    @Basic(optional = false)
    @Column(name = "product_name")
    private String productName;
    @Basic(optional = false)
    @Column(name = "product_cost")
    private int productCost;
    @Basic(optional = false)
    @Column(name = "product_quantity")
	private int productQuantity;
	@JsonIgnore
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Buy buy;

    public BuyLine() {
    }

    public BuyLine(Integer id) {
        this.id = id;
    }

    public BuyLine(Integer id, int idProduct, String productName, int productCost, int productQuantity) {
        this.id = id;
        this.idProduct = idProduct;
        this.productName = productName;
        this.productCost = productCost;
        this.productQuantity = productQuantity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getProductCost() {
        return productCost;
    }

    public void setProductCost(int productCost) {
        this.productCost = productCost;
    }

    public int getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(int productQuantity) {
        this.productQuantity = productQuantity;
    }

    public Buy getBuy() {
        return buy;
    }

    public void setBuy(Buy buy) {
        this.buy = buy;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof BuyLine)) {
            return false;
        }
        BuyLine other = (BuyLine) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication3.BuyLine[ id=" + id + " ]";
    }

}