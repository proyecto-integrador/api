package net.dcerp.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 *
 * @author David Caballero <d@dcaballero.net>
 */
@Entity
@Table(name = "buy", catalog = "dcerp", schema = "")
@EntityListeners(AuditingEntityListener.class)
@NamedQueries({
    @NamedQuery(name = "Buy.findAll", query = "SELECT b FROM Buy b")
    , @NamedQuery(name = "Buy.findById", query = "SELECT b FROM Buy b WHERE b.id = :id")
    , @NamedQuery(name = "Buy.findByProviderName", query = "SELECT b FROM Buy b WHERE b.providerName = :providerName")
    , @NamedQuery(name = "Buy.findByProviderAddress", query = "SELECT b FROM Buy b WHERE b.providerAddress = :providerAddress")
    , @NamedQuery(name = "Buy.findByProviderNumber", query = "SELECT b FROM Buy b WHERE b.providerNumber = :providerNumber")
    , @NamedQuery(name = "Buy.findByDate", query = "SELECT b FROM Buy b WHERE b.date = :date")})
public class Buy implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "provider_name")
    private String providerName;
    @Basic(optional = false)
    @Column(name = "provider_address")
    private String providerAddress;
    @Basic(optional = false)
    @Column(name = "provider_number")
    private int providerNumber;
    @Basic(optional = false)
    @Column(name = "date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "buy")
    private BuyLine buyLine;

    public Buy() {
    }

    public Buy(Integer id) {
        this.id = id;
    }

    public Buy(Integer id, String providerName, String providerAddress, int providerNumber, Date date) {
        this.id = id;
        this.providerName = providerName;
        this.providerAddress = providerAddress;
        this.providerNumber = providerNumber;
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public String getProviderAddress() {
        return providerAddress;
    }

    public void setProviderAddress(String providerAddress) {
        this.providerAddress = providerAddress;
    }

    public int getProviderNumber() {
        return providerNumber;
    }

    public void setProviderNumber(int providerNumber) {
        this.providerNumber = providerNumber;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BuyLine getBuyLine() {
        return buyLine;
    }

    public void setBuyLine(BuyLine buyLine) {
        this.buyLine = buyLine;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Buy)) {
            return false;
        }
        Buy other = (Buy) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication3.Buy[ id=" + id + " ]";
    }

}