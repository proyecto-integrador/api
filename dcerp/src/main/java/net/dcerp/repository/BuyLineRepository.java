package net.dcerp.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import net.dcerp.model.BuyLine;

public interface BuyLineRepository extends JpaRepository<BuyLine, String> {

}
