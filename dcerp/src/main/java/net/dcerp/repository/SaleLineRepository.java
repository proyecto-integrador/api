package net.dcerp.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import net.dcerp.model.SaleLine;

public interface SaleLineRepository extends JpaRepository<SaleLine, String> {

}
