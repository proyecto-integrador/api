package net.dcerp.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import net.dcerp.model.Sale;

public interface SaleRepository extends JpaRepository<Sale, String> {

}
