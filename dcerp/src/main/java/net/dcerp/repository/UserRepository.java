package net.dcerp.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import net.dcerp.model.User;

public interface UserRepository extends JpaRepository<User, Integer>{


}