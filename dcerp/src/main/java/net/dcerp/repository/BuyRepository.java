package net.dcerp.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import net.dcerp.model.Buy;

public interface BuyRepository extends JpaRepository<Buy, String> {

}
