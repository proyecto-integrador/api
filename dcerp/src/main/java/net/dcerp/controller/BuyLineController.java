package net.dcerp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.dcerp.dao.BuyLineDAO;
import net.dcerp.model.BuyLine;

@RestController
@RequestMapping("/api")
public class BuyLineController {


  @Autowired
BuyLineDAO buyLineDAO;

/* to save an Buy*/
@PostMapping("/buyLine-create")
public BuyLine createBuyLine(@Valid @RequestBody final BuyLine buyLine) {
  return buyLineDAO.save(buyLine);
}

/*get all buyLine*/
@GetMapping("/buyLine")
public List<BuyLine> getAllBuyLine(){
  return buyLineDAO.findAll();
}

  /* get Buy by buyLineId */
@GetMapping("/buyLine/{id}")
public ResponseEntity<BuyLine> getSBuyLineById(@PathVariable(value="id") final String buyLineId){

    final BuyLine buyLine= buyLineDAO.findOne(buyLineId);
  if(buyLine==null) {
    return ResponseEntity.notFound().build();
  }
  return ResponseEntity.ok().body(buyLine);
}


  /* update an Buy by buyLineId */
@PutMapping("/buyLine-update/{id}")
public ResponseEntity<BuyLine> updateBuyLine(@PathVariable(value="id") final String buyLineId,@Valid @RequestBody final BuyLine buyLineDetails){

  final BuyLine buyLine= buyLineDAO.findOne(buyLineId);
  if(buyLine==null) {
    return ResponseEntity.notFound().build();
  }
      //set of new values for attribute of the object
      buyLine.setId(buyLineDetails.getId());
      buyLine.setIdProduct(buyLineDetails.getIdProduct());
      buyLine.setProductName(buyLineDetails.getProductName());
      buyLine.setProductCost(buyLineDetails.getProductCost());
      buyLine.setProductQuantity(buyLineDetails.getProductQuantity());
      

    final BuyLine updateBuyLine = buyLineDAO.save(buyLine);
    return ResponseEntity.ok().body(updateBuyLine);

}

/*Delete an BuyLine*/
@DeleteMapping("/buyLine-delete/{id}")
public ResponseEntity<BuyLine> deleteBuy(@PathVariable(value="id") final String buyLineId){
  final BuyLine buyLine= buyLineDAO.findOne(buyLineId);
  if(buyLine==null) {
    return ResponseEntity.notFound().build();
  }
    buyLineDAO.delete(buyLine);
  return ResponseEntity.ok().build();
  }

}
