package net.dcerp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.dcerp.dao.BuyDAO;
import net.dcerp.model.Buy;

@RestController
@RequestMapping("/api")
public class BuyController {


  @Autowired
BuyDAO buyDAO;

/* to save an Buy*/
@PostMapping("/buy-create")
public Buy createBuy(@Valid @RequestBody final Buy buy) {
  return buyDAO.save(buy);
}

/*get all buy*/
@GetMapping("/buy")
public List<Buy> getAllBuy(){
  return buyDAO.findAll();
}

  /* get Buy by buyId */
@GetMapping("/buy/{id}")
public ResponseEntity<Buy> getSBuyById(@PathVariable(value="id") final String buyId){

    final Buy buy= buyDAO.findOne(buyId);
  if(buy==null) {
    return ResponseEntity.notFound().build();
  }
  return ResponseEntity.ok().body(buy);
}


  /* update an Buy by buyId */
@PutMapping("/buy-update/{id}")
public ResponseEntity<Buy> updateBuy(@PathVariable(value="id") final String buyId,@Valid @RequestBody final Buy buyDetails){

  final Buy buy= buyDAO.findOne(buyId);
  if(buy==null) {
    return ResponseEntity.notFound().build();
  }
      //set of new values for attribute of the object
      buy.setId(buyDetails.getId());
      buy.setProviderName(buyDetails.getProviderName());
      buy.setProviderAddress(buyDetails.getProviderAddress());
      buy.setProviderNumber(buyDetails.getProviderNumber());
      buy.setDate(buyDetails.getDate());
      

    final Buy updateBuy = buyDAO.save(buy);
    return ResponseEntity.ok().body(updateBuy);

}

/*Delete an Buy*/
@DeleteMapping("/buy-delete/{id}")
public ResponseEntity<Buy> deleteBuy(@PathVariable(value="id") final String buyId){
  final Buy buy= buyDAO.findOne(buyId);
  if(buy==null) {
    return ResponseEntity.notFound().build();
  }
    buyDAO.delete(buy);
  return ResponseEntity.ok().build();
  }

}
