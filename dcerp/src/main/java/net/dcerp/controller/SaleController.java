package net.dcerp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.dcerp.dao.SaleDAO;
import net.dcerp.model.Sale;

@RestController
@RequestMapping("/api")
public class SaleController {


  @Autowired
SaleDAO saleDAO;

/* to save an Sale*/
@PostMapping("/sale-create")
public Sale createSale(@Valid @RequestBody final Sale sale) {
  return saleDAO.save(sale);
}

/*get all sale*/
@GetMapping("/sale")
public List<Sale> getAllBuy(){
  return saleDAO.findAll();
}

  /* get Sale by SaleId */
@GetMapping("/sale/{id}")
public ResponseEntity<Sale> getSaleById(@PathVariable(value="id") final String saleId){

    final Sale sale= saleDAO.findOne(saleId);
  if(sale==null) {
    return ResponseEntity.notFound().build();
  }
  return ResponseEntity.ok().body(sale);
}


  /* update an Buy by saleId */
@PutMapping("/sale-update/{id}")
public ResponseEntity<Sale> updateSale(@PathVariable(value="id") final String saleId,@Valid @RequestBody final Sale saleDetails){

  final Sale sale= saleDAO.findOne(saleId);
  if(sale==null) {
    return ResponseEntity.notFound().build();
  }
      //set of new values for attribute of the object
      sale.setId(saleDetails.getId());
      sale.setCustomerName(saleDetails.getCustomerName());
      sale.setCustomerAddress(saleDetails.getCustomerAddress());
      sale.setCustomerNumber(saleDetails.getCustomerNumber());
      sale.setDate(saleDetails.getDate());
      

    final Sale updateSale = saleDAO.save(sale);
    return ResponseEntity.ok().body(updateSale);

}

/*Delete a Sale*/
@DeleteMapping("/sale-delete/{id}")
public ResponseEntity<Sale> deleteSale(@PathVariable(value="id") final String saleId){
  final Sale sale= saleDAO.findOne(saleId);
  if(sale==null) {
    return ResponseEntity.notFound().build();
  }
    saleDAO.delete(sale);
  return ResponseEntity.ok().build();
  }

}
