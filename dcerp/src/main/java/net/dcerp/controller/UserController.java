package net.dcerp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import net.dcerp.dao.UserDAO;
import net.dcerp.model.User;



@RestController
@RequestMapping("/api")
public class UserController {

	@Autowired
	UserDAO userDAO;

	/* to save an User*/
	@PostMapping("/user-create")
    @CrossOrigin(origins = "*")
    @ApiOperation(value = "Create User", response = Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User Created", response = Page.class),
            @ApiResponse(code = 400, message = "Invalid request"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
	public User createUser(@Valid @RequestBody User user) {
		return userDAO.save(user);
	}

	/*get all Users*/
	@GetMapping("/user")
    @CrossOrigin(origins = "*")
    @ApiOperation(value = "Get All Users", response = Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Users found", response = Page.class),
            @ApiResponse(code = 400, message = "Invalid request"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
	public List<User> getAllUsers(){
		return userDAO.findAll();
	}

	/*get User by userid*/
	@GetMapping("/user/{id}")
    @CrossOrigin(origins = "*")
    @ApiOperation(value = "Get User by ID", response = Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User found", response = Page.class),
            @ApiResponse(code = 400, message = "Invalid request"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
	public ResponseEntity<User> getUserById(@PathVariable(value="id") Integer userId){

		User user=userDAO.findOne(userId);
		if(user==null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(user);
	}


	/*update an User by userid*/
	@PutMapping("/user-update/{id}")
    @CrossOrigin(origins = "*")
    @ApiOperation(value = "Update User", response = Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User updated", response = Page.class),
            @ApiResponse(code = 400, message = "Invalid request"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
	public ResponseEntity<User> updateUser(@PathVariable(value="id") Integer userId,@Valid @RequestBody User userDetails){

		User user=userDAO.findOne(userId);
		if(user==null) {
			return ResponseEntity.notFound().build();
		}
        //set of new values for attribute of the object
        user.setUserName(userDetails.getUserName());
        user.setUserPassword(userDetails.getUserPassword());

		User updateUser=userDAO.save(user);
		return ResponseEntity.ok().body(updateUser);
	}

	/*Delete an User*/
	@DeleteMapping("/user-delete/{id}")
    @CrossOrigin(origins = "*")
    @ApiOperation(value = "User deleted", response = Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "User deleted", response = Page.class),
            @ApiResponse(code = 400, message = "Invalid request"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
	public ResponseEntity<User> deleteUser(@PathVariable(value="id") Integer userId){

		User user=userDAO.findOne(userId);
		if(user==null) {
			return ResponseEntity.notFound().build();
		}
		userDAO.delete(user);
		return ResponseEntity.ok().build();
    }
}