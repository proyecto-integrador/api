package net.dcerp.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.dcerp.dao.SaleLineDAO;
import net.dcerp.model.SaleLine;

@RestController
@RequestMapping("/api")
public class SaleLineController {


  @Autowired
SaleLineDAO saleLineDAO;

/* to save an SaleLine*/
@PostMapping("/saleLine-create")
public SaleLine createSaleLine(@Valid @RequestBody final SaleLine saleLine) {
  return saleLineDAO.save(saleLine);
}

/*get all saleLine*/
@GetMapping("/saleLine")
public List<SaleLine> getAllBuy(){
  return saleLineDAO.findAll();
}

  /* get SaleLine by SaleLineId */
@GetMapping("/saleLine/{id}")
public ResponseEntity<SaleLine> getSaleLineById(@PathVariable(value="id") final String saleLineId){

    final SaleLine saleLine= saleLineDAO.findOne(saleLineId);
  if(saleLine==null) {
    return ResponseEntity.notFound().build();
  }
  return ResponseEntity.ok().body(saleLine);
}


  /* update an Buy by saleLineId */
@PutMapping("/saleLine-update/{id}")
public ResponseEntity<SaleLine> updateSaleLine(@PathVariable(value="id") final String saleLineId,@Valid @RequestBody final SaleLine saleLineDetails){

  final SaleLine saleLine= saleLineDAO.findOne(saleLineId);
  if(saleLine==null) {
    return ResponseEntity.notFound().build();
  }
      //set of new values for attribute of the object
      saleLine.setId(saleLineDetails.getId());
      saleLine.setIdProduct(saleLineDetails.getIdProduct());
      saleLine.setProductName(saleLineDetails.getProductName());
      saleLine.setProductCost(saleLineDetails.getProductCost());
      saleLine.setProductQuantity(saleLineDetails.getProductQuantity());

      

    final SaleLine updateSaleLine = saleLineDAO.save(saleLine);
    return ResponseEntity.ok().body(updateSaleLine);

}

/*Delete a BuyLine*/
@DeleteMapping("/saleLine-delete/{id}")
public ResponseEntity<SaleLine> deleteSaleLine(@PathVariable(value="id") final String saleLineId){
  final SaleLine saleLine= saleLineDAO.findOne(saleLineId);
  if(saleLine==null) {
    return ResponseEntity.notFound().build();
  }
    saleLineDAO.delete(saleLine);
  return ResponseEntity.ok().build();
  }

}
