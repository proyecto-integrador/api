package net.dcerp.dao;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.dcerp.model.Buy;
import net.dcerp.repository.BuyRepository;

@Service
public class BuyDAO {

	@Autowired
	BuyRepository buyRepo;

/* save a Buy*/

	public Buy save(Buy buy) {

		return buyRepo.save(buy);

	}


/* search all Buy */

	public List<Buy> findAll(){

		return buyRepo.findAll();

	}

/* get a Buy by code */

	public Buy findOne(String buyId) {
		return buyRepo.findById(buyId).orElse(null);
	}

/*delete a Buy*/

	public void delete(Buy buy) {
		buyRepo.delete(buy);
	}



}
