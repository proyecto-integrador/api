package net.dcerp.dao;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.dcerp.model.SaleLine;
import net.dcerp.repository.SaleLineRepository;

@Service
public class SaleLineDAO {

	@Autowired
	SaleLineRepository saleLineRepo;

	/* save an saleLine*/

	public SaleLine save(SaleLine saleLine) {

		return saleLineRepo.save(saleLine);

	}


	/* search all saleLine */
	
	public List<SaleLine> findAll(){

		return saleLineRepo.findAll();

	}

	/* get an store by saleLine */

	public SaleLine findOne(String saleLineId) {
		return saleLineRepo.findById(saleLineId).orElse(null);
	}

	/* delete an saleLine */

	public void delete(SaleLine saleLine) {
		saleLineRepo.delete(saleLine);
	}



}
