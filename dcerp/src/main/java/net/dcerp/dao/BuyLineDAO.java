package net.dcerp.dao;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.dcerp.model.BuyLine;
import net.dcerp.repository.BuyLineRepository;

@Service
public class BuyLineDAO {

	@Autowired
	BuyLineRepository buyLineRepo;

/* save a BuyLine*/

	public BuyLine save(BuyLine buyLine) {

		return buyLineRepo.save(buyLine);

	}


/* search all BuyLine */

	public List<BuyLine> findAll(){

		return buyLineRepo.findAll();

	}

/* get a Buy by id */

	public BuyLine findOne(String buyId) {
		return buyLineRepo.findById(buyId).orElse(null);
	}

/*delete a BuyLine*/

	public void delete(BuyLine buyLine) {
		buyLineRepo.delete(buyLine);
	}



}
