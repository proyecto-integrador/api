package net.dcerp.dao;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.dcerp.model.User;
import net.dcerp.repository.UserRepository;

@Service
public class UserDAO {

	 @Autowired
		UserRepository userRepository;
		
		/*to save an User*/
		public User save(User user) {
			return userRepository.save(user);
		}
		
		/* search all Users*/
		public List<User> findAll(){
			return userRepository.findAll();
		}
		
		/*get an User by id*/
		public User findOne(Integer userId_user) {
			return userRepository.findById(userId_user).orElse(null);
		}
		
		/*delete an User*/      
		public void delete(User user) {
			userRepository.delete(user);
		}
		
}