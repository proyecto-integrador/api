package net.dcerp.dao;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.dcerp.model.Sale;
import net.dcerp.repository.SaleRepository;

@Service
public class SaleDAO {

	@Autowired
	SaleRepository saleRepo;

	/* save an sale */

	public Sale save(Sale sale) {

		return saleRepo.save(sale);

	}


	/* search all sale */
	
	public List<Sale> findAll(){

		return saleRepo.findAll();

	}

	/* get an store by sale */

	public Sale findOne(String saleId) {
		return saleRepo.findById(saleId).orElse(null);
	}

	/* delete an sale */

	public void delete(Sale sale) {
		saleRepo.delete(sale);
	}



}
